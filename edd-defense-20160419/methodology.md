## Methodology

### Design-based Research

* First iteration: Malhotra and Zhou (2012)

* Second iteration: Pilot study (Zhou, 2015)

* Third iteration: The present study


### Data Collection

* Public discussions (estimated literature body)
  * Flipped classroom
  * Classroom management
  <li class="fragment" data-fragment-index="0">Literature was collected from 2004-2013 top journals ranked by the *journal impact factor* (Thompson Reuters, 2015)</li>
* Course (accurate literature body)
  * Sociology of evaluation
  <li class="fragment" data-fragment-index="0">Literature was collected based on the instructor-provided syllabus</li>

<div class="fragment">
  <ul>
    <li>Control group theme</li>
  </ul>
</div>


### Data Analysis

<p class="text-left">
  *Latent Dirichlet Allocation* (LDA) (Blei, Ng, & Jordan, 2003; Blei and Lafferty, 2009) was used to identify the underlying relations between the discussions and learning resources.
</p>

<div class="five-hundred-w pull-left" style="margin-top: -10px !important;">
  <img src="images/discussion-types.jpg" />
</div>

<div class="pull-right text-left four-twenty-five-w small-font">
  <p>similarity threshold</p>
  <ul>
    <li>High: ≥ 0.9</li>
    <li>Low: ≤ 0.1</li>
  </ul>
  <p>discussions in focus</p>
  <ul>
    <li>Type I</li>
    <li>Type II</li>
  </ul>
</div>
