## Purpose of the study

<ul>
  <li class="fragment">
    Propose the **VICTOR (VIdeo-Course-Teacher-discOuRse)** framework as a holistic, flexible, and scalable approach for understanding and evaluating video-based curricula in teacher education or other subjects in general.
  </li>

  <li class="fragment">
    Pilot the **topic models (TM)** method as the analytical procedure for discovering underlying relations between the professional discourse and the learning resources.
  </li>
</ul>
