<h2 class="no-text-transform">VICTOR: VIdeo-Course-Teacher-discOuRse</h2>

<div class="pull-left five-fifty-w">
![VICTOR Framework](images/victor.png "VICTOR (VIdeo-Course-Teacher-discOuRse)")
</div>

<div class="pull-right three-fifty-w text-left pad-top">
  <p class="small-font">Inspiring literature: </p>
  <ul class="small-font">
    <li>Hill and Hannafin (2001)</li>
    <li>Koehler and Mishra (2009)</li>
    <li>Shaffer, et al. (2009)</li>
    </ul>
  <p class="small-font">
    Analytical procedure: topic models (TM)
  </p>
  <p class="small-font">
    Preexisting knowledge was neglected in this study due to the lack of effective measure.
  </p>
</div>

<aside class="notes">
  <p>why do I think VICTOR is holistic, flexible, scalable approach?</p>
  <p>applications: evaluate discussion; evaluate curriculum design; assess student needs</p>
  <p>Hill & Hannafin: resurgence of resource-based learning environment (RBLE). Static resources; dynamic resources (peer learners); contexts; externally directed (teacher, instructional designers); learner-generated; tools; scaffolds, etc.</p>
  <p>Koehler & Mishra: TPACK - Content, Technology, Pedagogy</p>
  <p>Shaffer et al.: epistemic frame theory (EFT); epistemic network analysis (ENA); epistemic games</p>
</aside>


## Research questions

<div class="text-left center-960px-w">
  <h3>Primary question</h3>
  <p>According to the VICTOR framework, do primary, course-related learning resources contribute to video-driven professional education discussions? If so, in what way(s)?</p>
</div>

<div class="text-left center-960px-w">
  <h3>Secondary question</h3>
  <p>Can topic models identify the underlying relations between video-based discussion and course-related learning resources effectively and consistently?</p>
</div>
