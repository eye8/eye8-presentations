<aside class="notes">
  <p>Design-based approach</p>
  <p>Why I am motivated to conduct research on this topic?<p>
  <ul>
    <li>Vialogue for reflective critical learning around videos</li>
    <li>primary design goal for teacher training</li>
    <li>lack of an flexible, scalable solution for evaluating discussions</li>
  </ul>
</aside>

## Video, Dialogues, Learning, & Evaluation

<div class="slideshow">
  <div>
    <img src="images/metacognition.jpg" />
  </div>
  <div>
    <a href="https://vialogues.com/" target="_blank">
      <img src="images/vialogue.png" />
    </a>
  </div>
  <div><img src="images/vialogues-stats.png" /></div>
  <div><img src="images/evaluation.png" /></div>
</div>


## Problem Statement

<p class="text-left">
Video-driven teacher education curricula cannot be adequately understood or effectively assessed due to the lack of a holistic approach in curriculum design and evaluation.
</p>

<p class="small-font text-left">
  Related literature: Hill and Hannafin (2001); Koehler and Mishra (2009); Masats and Dooly (2011); Wang and Hartley (2003).
</p>

<aside class="notes">
  <p>Hill & Hannafin: resurgence of resource-based learning environment (RBLE). Static resources; dynamic resources (peer learners); contexts; externally directed (teacher, instructional designers); learner-generated; tools; scaffolds, etc.</p>
  <p>Koehler & Mishra: TPACK - Content, Technology, Pedagogy</p>
  <p>Masats & Dooly: four-pronged video case models where learners experience both teacher and student roles: 1) re-winding; 2) zooming in; 3) freeze-framing; 4) bird's eye-viewing</p>
  <p>Wang & Hartley: modeling, microteaching, interaction analysis, video cases, field recording, hypermedia program</p>
</aside>
