## Limitations

* Teacher preexisting knowledge was not included.
* Limited number of themes were analyzed.
* LDA model was not optimized.
* Appropriate criteria are needed for Type I, II, III, and IV discussions in order to use those categories to indicate the type of learning.
