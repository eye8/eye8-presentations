## Outline

1. [The problem statement](#/3)
1. [The purpose of the study](#/4)
1. [Theoretical framework](#/5)
1. [Methodology](#/6)
1. [Data collection](#/7)
1. [Results](#/8)
1. [Discussions](#/9)
1. [Questions](#/10)
