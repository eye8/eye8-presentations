## Case III

#### Vialogue title: *Driving Test in the UK (Pt. 2) - Soc Eval Discussion*

<p>
  @00:00-
</p>
<div class="vialogues-player">
    <div id="case_study_3"></div>
</div>


#### Comment #619
> @00:00:<br />
  Could not get video to play...


### Similar literature article (similarity≥0.9)

* Gladwell, M. (2002a). The naked face. The New Yorker.
* Gladwell, M. (2002b). The talent myth. The New Yorker.
* Gladwell, M. (2004). Personality plus.  The New Yorker.
* Gladwell, M.  (2005a).  Blink: The Power of Thinking Without Thinking. New York, NY:  Little Brown and Company.
* Gladwell, M. (2008). Most likely to succeed. The New Yorker.
