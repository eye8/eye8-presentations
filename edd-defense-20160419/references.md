## References

Blei, D. M., Lafferty, J. D. (2009). Topic models. In A. Srivastava & M. Sahami (Eds.), Text Mining: Classification, Clustering, and Applications (pp. 71-94). Boca Raton, FL: Taylor & Francis Group.

Blei, D. M., Ng, A. Y., and Jordan, M. I. (2003). Latent Dirichlet Allocation, The Journal of Machine Learning Research, Vol. 3, 993-1022.

Guo, W., & Diab, M. (2012). Modeling Sentences in the Latent Space. In Proceedings of the 50th Annual Meeting of the Association for Computational Linguistics (pp. 864-872). Association for Computational Linguistics.

Guo, W., Li, H., Ji, H., & Diab, M. (2013). Linking Tweets to News: A Framework to Enrich Short Text Data in Social Media. In Proceedings of the 51st Annual Meeting of the Association for Computational Linguistics (pp. 239-249).


Hill, J., & Hannafin, M (2001). Teaching and learning in digital environments: The resurgence of resource-based learning. Educational Technology Research and Development, 49(3), 37-52.

Koehler, M., & Mishra, P. (2009). What is Technological Pedagogical Content Knowledge (TPACK)? Contemporary Issues in Technology and Teacher Education, 9(1), 60-70.

Malhotra, M., & Zhou, Z. (2012, June). Understanding usage patterns of video-driven discussion using singular value and semi-discrete decomposition. Paper presented at the World Conference on Educational Multimedia, Hypermedia and Telecommunications, Denver, CO.

Masats, D., & Dooly, M. (2011). Rethinking the use of video in teacher education: A holistic approach. Teaching and Teacher Education, 27(7), 1151-1162.


Naveed, N., Gottron, T., Kunegis, J., & Alhadi, A. C. (2011). Searching microblogs: coping with sparsity and document quality. In Proceedings of the 20th ACM International Conference on Information and Knowledge Management (pp. 183-188). ACM.

Puniyani, K., Eisenstein, J., Cohen, S., & Xing, E. P. (2010). Social Links from Latent Topics in Microblogs. In Proceedings of the NAACL HLT 2010 Workshop on Computational Linguistics in a World of Social Media (pp. 19–20). Association for Computational Linguistics.

Shaffer, D., Hatfield, D., Svarovsky, G., Nash, P, Nulty, A., Bagley, E., Frank, K., Rupp, A., & Mislevy, R. (2009). Epistemic Network Analysis: A prototype for 21st Century assessment of learning. The International Journal of Learning and Media. 1(2), 33-53.

Vygotsky, L. S. (1978). Zone of proximal development: A new approach. Mind in Society: The development of higher psychological processes (pp. 84-91). Cambridge, MA: Harvard University Press.


Wang, J., & Hartley, K. (2003). Video technology as a support for teacher education reform. Journal of Technology and Teacher Education, 11(1), 105-138.

Zhou, Z. (2015). Pilot study of using topic models for text mining video-driven discussions. Unpublished pilot study conducted at Teachers College, Columbia University.
