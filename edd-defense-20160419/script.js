// $(function() {

  var installCaseStudyVideo = function(divId, vialogueId) {
    var playerInst = jwplayer(divId);
    if (!playerInst.getState()) {
      $.ajax({
          url: 'https://vialogues.com/api/vialogues/' + vialogueId,
          timeout: 500,
          success: function(data) {
            var file;
            if (data.video.service === 'amazon') {
              file = data.video.files.original.video_uri;
            } else {
              file = 'https://youtube.com/watch?v=' + data.video.files.original.key;
            }
            playerInst.setup({
              file,
              image: data.video.icon,
              width: 450,
              height: 253
            });
          },
          error: function(jqXHR, textStatus) {
              alert("Unable to obtain video info");
          }
      });
    }
  };

  Reveal.addEventListener('slidechanged', function(evt) {
    switch (evt.indexh) {
    case 3:
      $('.slideshow').not('.slick-initialized').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        speed: 500,
        fade: true,
        centerMode: true,
        infinite: true
      });
      break;
    case 7:
      if (evt.indexv === 0) {
        installCaseStudyVideo("case_study_1", 29178);
      }
      break;
    case 8:
      if (evt.indexv === 0) {
        installCaseStudyVideo("case_study_2", 26766);
      }
      break;
    case 9:
      if (evt.indexv === 0) {
        installCaseStudyVideo("case_study_3", 112);
      }
      break;
    case 16:
      if (evt.indexv === 0) {
        installCaseStudyVideo("case_study_4", 150);
      }
      break;
    }
  });

// });
