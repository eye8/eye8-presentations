## An Analysis Framework for Video-Based Teacher Education Curriculum
## Through the Lens of Topic Modeling

#### Zhou Zhou ∙ Xiang Liu ∙ Hui Soo Chae ∙ Gary Natriello
#### EdLab, Teachers College of Columbia University

Paper presented at AERA, April 9, 2016, Washington D.C.

<a href="http://edlab.tc.columbia.edu" target="_blank">
  <img src="images/edlab-red.png" class="no-border" />
</a>
