## Case II

<h4>
  Vialogue title: *Sixth Grade Classroom Management*
</h4>
<p>
  @2:06-2:34
</p>
<div class="vialogues-player">
    <div id="case_study_2"></div>
</div>


### Comment #6040

> @2:10:<br />
  I like the way she energizes and engages her students so they're giving her their full attention, but its almost like they're in military school. haha.

#### Discussion-transcript similarity = 0.0037


### Comment Thread #3487

> @6:00 (video playing credits)<br />
  (Discussant A): What barriers do you think you would face in trying to implement these techniques in your classroom? <br />
  (Discussant B): Seems the teacher devotes too much class time to rehearsing the gestures and other movements. I also doubt how effective that is in terms of learning. For instance, having over 30 kids all talking at once is not my idea of a good learning opportunity

#### Discussion-transcript similarity = 0.0032
