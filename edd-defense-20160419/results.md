## Results

### Most topic model predictions were accurate but not consistently reliable.

* Most similar documents appeared to be relevant, but some didn't. [[similar vs relevant]](#/5/1)
* Many dissimilar documents appeared to be relevant.
* Many control-group theme discussions were similar to the experiment group theme literatures.


### Similarity VS relevance

* *Similarity* refers to the semantic resemblance between two documents (e.g. a discussion vs a literature article, or a discussion vs. a video transcript) as judged by topic models.

* *Relevance* refers to the relatedness between two documents as judged by the researcher.


### Causes of the inconsistent predictions

* Online discussions are *noisy* (e.g. Naveed et al., 2011).
* Noisy data lead to *generic* terms and topics.
* Generic topics cause Type I errors (false positive/similarity).
* Tuning the topic model could have helped improve the noise problem (e.g. Guo & Diab, 2012; Guo, et al., 2013; Puniyani, et al., 2010), which would be a future direction along this research line.

<aside class="notes">
  <p>unimportant term examples: <i>the</i>, <i>is</i>, <i>by</i>, etc.</p>
  <p>Puniyani, et al. (2010) described a method similar to the by-thread and by-vialogue approaches in this thesis, where the authors gathered together all the tweets of a given Twitter user into a single document on which LDA was performed.</p>
  <p>Guo and Diab (2012) proposed the Weighted Textual Matrix Factorization (WTMF) model which attempted to improve the topic model’s reliability by assigning small weights for the missing words in tweets.</p>
  <p>Guo, Li, Ji, & Diab (2013) applied the WTMF model to uncover the relations between tweets and news paragraphs.</p>
</aside>
