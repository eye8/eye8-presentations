## Applications

<div class="forty-five-percent-w pull-left pad-top">
  <ul>
    <li>Evaluate the meaningfulness of video-driven discussions.</li>
    <li>Evaluate the effectiveness of curriculum design.</li>
    <li>Assess real-time student needs for pedagogical support.</li>
  </ul>
</div>

<div class="five-hundred-w pull-right">
  ![ZPD](images/zpd.jpg "Zone of Proximal Development")
  <p class="small-font" style="margin-top: -10px;">Zone of Proximal Development (Vygotsky, 1978)</p>
</div>


### Data Collection

|Theme|Total # Vilogues|Total # Comments|Total # Literatures|
|---|---|---|---|---|
|Flipped Classroom|12|374|27|
|Classroom Management|16|1125|124|
|Sociology of Evaluation|12|194|136|
