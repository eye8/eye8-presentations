## Conclusions

### VICTOR is helpful for understanding and evaluating video-based curriculum.


### VICTOR can help monitor the learning outcome and evaluate the need of pedagogical support.


### The analytical procedure (i.e. topic models or alternatives) requires significant improvement in order to make consistently reliable predictions.

* TM is good at discovering document similarities in terms of low-level semantic functions, but not high-level semantic functions or knowledge domain transfers.
* The analytical procedure requires improvement in order to make consistently reliable predictions.
