## Case I

<h4>
  Vialogue title: *Using videos to flip the classroom: Salman Khan and Reinventing Education*
</h4>
<p>
  @14:04-14:35
</p>
<div class="vialogues-player">
    <div id="case_study_1"></div>
</div>


### Comment #110131
> @14:34:<br />
  "It really makes you see how all of the labels...were really just a coincidence of time." Although I was one of those individuals who learned quickly, and therefore was labeled as "smart," I have never felt intelligent in that sort of quintessential manner. Speed should not equate to understand; in fact, I believe that the opposite should be true.

#### Discussion-transcript similarity = 0.9985


### Similar literature article

> Sams, A., & Bergmann, J. (2011). Flipping the Classroom. Educational Horizons Magazine, Vol. 90, 5-7

#### Discussion-literature similarity = 0.9853
