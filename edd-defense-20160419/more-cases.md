## More Cases

<h4>
  Vialogue title: *Snap Judgment - Soc Eval Discussion*
</h4>
<p>
  @00:10-
</p>
<div class="vialogues-player">
    <div id="case_study_4"></div>
</div>


### Comment #723
> @00:00:<br />
  Three thoughts (applications):<br />
  1. “Gut feelings” <br />
  2. “You have less than 20 seconds to make a good first impression.” <br />
  3. Speed dating


### Comment #728
> @05:03:<br />
  It is quite interesting when he suggests that taking away visual information can make someone a better decision maker. Perhaps because I'm going through it, I think about how the job interview process is the complete opposite of making quick decisions. Candidates generally go through many long interviews. Would it more productive to make a snap judgement?


### Similar literature article (similarity≥0.9)

|Literature|Comment|
|---|---|
|Gladwell, M. (2002a). The naked face. The New Yorker.|#723, #728|
|Gladwell, M. (2002b). The talent myth. The New Yorker.|#723|
|Gladwell, M. (2004). Personality plus.  The New Yorker.|#723, #728|
|Gladwell, M.  (2005a).  Blink: The Power of Thinking Without Thinking. New York, NY:  Little Brown and Company.|#723, #728|
|Gladwell, M. (2008). Most likely to succeed. The New Yorker.|#723|
