## Outline

1. [Video, Dialogues, Learning, & Evaluation](#/3)
1. [VICTOR: Video-Course-Teacher-discOuRse](#/4)
1. [Methodology](#/5)
1. [Results](#/6)
1. [Cases](#/7)
1. [Conclusions](#/8)
1. [Limitations](#/9)
1. [Future Research](#/10)
1. [Q&A](#/13)
<li><a style="color:gray;" href="#/14">References</a></li>
<li><a style="color:gray;" href="#/15">Resources</a></li>
<li><a style="color:gray;" href="#/16">More discussion cases</a></li>
