## Future Research

* Improve analytical procedures
  * Improve LDA/TM in order to make consistently reliable predictions.
  * Discover alternative methods that might work better in different contexts.
* Inform instructional design
  * Improve curriculum design based on the VICTOR framework analysis.
  * Real-time discussion assessment and intelligent agent.
