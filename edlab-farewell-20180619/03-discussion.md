## Group Discussion (15 min)

1. Think about the four dimensions (Education, Technology, Research, Practice) of the EdLab products. Which project(s) is the ideal type to be focused on at EdLab?

2. How should EdLab make decisions in selecting projects in order to make us innovative, down-to-earth, efficient, and forward-thinking?

3. Name an existing or new project your group would like to focus on based on Q1 and Q2.
