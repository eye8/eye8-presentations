## Zhou's Journey @EdLab


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2008">2008</div>
    <div class="edlab-projects">
      <h3>EdLab Projects</h3>
      <p class="project"><i>Teaching the Levees</i> w/ Rockefeller Foundation</p>
      <p class="project"><i>Watch It And Learn!</i> w/ HBO</p>
    </div>
  </div>
</div>
<div class="small-font my-projects">
  <h3>Zhou's Projects</h3>

  <p class="project">Interactive Video Visualization Tool (IVVT)</p>
  <p>- For <i>Watch It And Learn!</i></p>
  <p>- Predecessor of Vialogues</p>
</div>


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2009">2009</div>
    <div class="edlab-projects">
      <h3>EdLab Projects</h3>
      <p class="project">Math Maze</p>
    </div>
  </div>
</div>
<div class="small-font my-projects">
  <h3>Zhou's Projects</h3>
  <p class="project">IVVT &rArr; Critter</p>
  <p>- 1st iteration of Vialogues</p>
  <p>- Built with Flash</p>
  <p class="project">TC Route Finder (TCRF)</p>
  <p class="project">Anwar, Zhou, Shi, & Chae (2009, May). Critter: Social Reflective Learning Around Web-Based Video. TCETC, New York, NY.</p>
</div>


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2010">2010</div>
    <div class="edlab-projects">
      <h3>EdLab Projects</h3>
      <p class="project">Pressible</p>
      <p class="project">EdNode</p>
    </div>
  </div>
</div>
<div class="small-font my-projects">
  <h3>Zhou's Projects</h3>
  <p class="project">Critter &rArr; Vialogues</p>
  <p>2nd iteration of Vialogues</p>
  <p>- Built with HTML, CSS, and jQuery</p>
  <p>- Restful API</p>
  <p class="project">Choi, Anwar, Zhou, etc. (2010, April). Evaluating an asynchronous video discussion tool: Results from a pilot study of "Critter". AERA, Denver, CO.</p>
</div>


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2011">2011</div>
    <div class="edlab-projects">
      <h3>EdLab Projects</h3>
      <p class="project">Research Broker</p>
      <p class="project">Pundit</p>
    </div>
  </div>
</div>
<div class="small-font my-projects">
  <h3>Zhou's Projects</h3>
  <p class="project">Zhou, Yuan, Chae, & Agnitti (2011, April). iPad e-reader apps: How effectively do they support academic work? AERA, New Orleans, LA.</p>
  <img src="images/eugene.jpg" height="220" />
</div>


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2012">2012</div>
    <div class="edlab-projects">
      <h3>EdLab Projects</h3>
      <p class="project">New Learning Times 1st iteration (participate)</p>
      <p class="project">NLT Solr recommendation</p>
      <p class="project">Data Dashboard tracking</p>
    </div>
  </div>
</div>
<div class="small-font my-projects">
  <h3>Zhou's Projects</h3>
  <p class="project"><i>Rock & Roll Forever</i> w/ Rock & Roll Forever Foundation</p>
  <p class="project">Vialogues Player 1st iteration w/ Flowplayer</p>
  <p class="project">Malhotra & Zhou (2012, June). Understanding usage patterns of video-driven discussion using singular value and semi-discrete decomposition. EdMedia, Denver, CO.</p>
</div>


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2013">2013</div>
    <div class="edlab-projects">
      <h3>EdLab Projects</h3>
      <p class="project">Roomer</p>
      <p class="project">mSchool (predecessor of Rhizr)</p>
    </div>
  </div>
</div>
<div class="small-font my-projects">
  <h3>Zhou's Projects</h3>
  <p class="project"><i>YoungArts Master Class</i> w/ National Young Arts Foundation</p>
  <p class="project">Vialogues Player w/ CC, sharing, embedding</p>
  <p class="project">SEO for Vialogues & NLT</p>
  <p class="project">Zhou (2013, April). Connecting teacher bloggers: Unleashing the educational power of Wordpress. Sloan Consortium, Las Vegas, NV.</p>
</div>


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2014">2014</div>
    <div class="edlab-projects">
      <h3>EdLab Projects</h3>
    </div>
  </div>
</div>
<div class="small-font my-projects">
  <h3>Zhou's Projects</h3>
  <p class="project"><i>Understanding Fiscal Responsibility</i> w/ Peter G. Peterson Foundation</p>
  <p>- Teacher Network</p>
  <p class="project">Vialogues 3rd iteration</p>
  <p>- Totally redesigned UI</p>
  <p class="project">Upgrading & migration: Pressible, Teaching the Levees, Let Freedom Swing</p>
</div>


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2015">2015</div>
    <div class="edlab-projects">
      <h3>EdLab Projects</h3>
      <p class="project">Data Dashboard w/ JupyterHub (participate)</p>
    </div>
  </div>
</div>
<div class="small-font my-projects">
  <h3>Zhou's Projects</h3>
  <p class="project">New Learning Times (NLT) 2nd iteration</p>
  <p>- CMS upgrade</p>
  <p>- Totally redesigned UI</p>
  <p class="project">Upgrading & migration</p>
  <p>- EdLab website</p>
  <p>- Teachers College Library</p>
</div>


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2016">2016</div>
    <div class="edlab-projects">
      <h3>EdLab Projects</h3>
      <p class="project">Smith Learning Theater website (participate)</p>
      <p class="project">Rhizr (participate)</p>
      <p class="project">Roomer (bug fix)</p>
    </div>
  </div>
</div>
<div class="small-font my-projects">
  <h3>Zhou's Projects</h3>
  <p class="project">Dockerization of Vialogues & NLT</p>
  <p class="project">EdLab Supersearch PoC</p>
  <p>- Find hidden relations between different projects</p>
  <p>- Adaptive learning</p>
  <p>- Cross promotion</p>
  <p class="project">Vialogues Player 2nd iteration w/ JWPlayer</p>
</div>


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2016">2016</div>
    <div class="edlab-projects">
    </div>
  </div>
</div>
<div class="small-font my-projects">
  <p class="project">Zhou, Liu, Chae, & Natriello (2016). An analysis framework for video-based teacher education curriculum through the lens of topic modeling. AERA, Washington D.C.</p>
  <img src="images/graduation.jpg" width="300" />
</div>


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2017">2017</div>
    <div class="edlab-projects">
      <h3>EdLab Projects</h3>
      <p class="project">Quuppa server deployment (participate)</p>
    </div>
  </div>
</div>
<div class="small-font my-projects">
  <h3>Zhou's Projects</h3>
  <p class="project">Vialogues 4th iteration</p>
  <p>- Totally redesigned UI</p>
  <p>- React/Redux full-js stack with synchronous discussion support</p>
  <p class="project">NLT 3rd iteration</p>
  <p>- Backend overhaul</p>
  <p class="project">EdLab Supersearch</p>
  <p>- Dev and integration with the new EdLab website, Vialogues, and NLT</p>
</div>


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2018">2018</div>
    <div class="edlab-projects">
      <h3>EdLab Projects</h3>
      <p class="project">New TC Library website & app (participate)</p>
    </div>
  </div>
</div>
<div class="small-font my-projects">
  <h3>Zhou's Projects</h3>
  <p class="project">EdLab Messenger</p>
  <p>- Newsletter microservice for NLT, TCR, Library, etc.</p>
  <p>- Prerequisite for personalized and adaptive content</p>
  <p class="project">Vialogues Player upgrade</p>
</div>
