### Field Recordings / Video Feedback

#### Purpose

* Professional assessment.

* Receive peer/administrator feedback.

* Promote reflective and collaborative learning in communities of practice.

Note:
==== 8 min ====
* The purpose and procedures of field recordings have been constantly shifting since the introduction of video in PD.


#### Procedures

![Field Recordings Procedures](images/field-recording.png)
<p class="small-font">*Field Recordings/Video Feedback Workflow*</p>

Note:
* A lot of similarities between field recording, micropractice, and video cases
    * watch video alone or with peers
    * self-observation or receive community support
* Their distinctions are:
    * Micropractice is mainly for pre-service professionals to learn and improve skills and problem-solving strategies
    * Video cases are for critical reflection on other people's problem solving (effective or ineffective)
    * Field recordings are for pre-service or novice professionals to critically reflect on others' or their own performance in order to make improvements


#### Anchored self-observation on second-language teaching

<div class="streach">
    <iframe src="https://vialogues.com/vialogues/play_embedded/15588/" width="60%" height="600" frameBorder="0" scrolling="No" allowfullscreen></iframe>
</div>

Note:
* TESOL students field teaching in Community English Program at Teachers College
* No need to play video. Scroll down discussion to show it's self-observation


#### Anchored feedback on teacher interacting with students

<div class="streach">
    <iframe src="https://vialogues.com/vialogues/play_embedded/28285/" width="60%" height="600" frameBorder="0" scrolling="No" allowfullscreen></iframe>
</div>

Note:
* High school teacher shared her class teaching field recording and collected feedback on her effectiveness of providing feedback to students
* 01:53 colleague (mentor or instructor) gives feedback on Meredith's nonchalant approach


#### Brief Summary So Far

<ul>
    <li class="fragment roll-in">
        The goals and procedures of video-based pedagogical models change over time.
    </li><br />
    <li class="fragment roll-in">Common trends
        <ul>
            <li>Community support.</li>
            <li>Constructive and adaptive learning.</li>
            <li>Increasing use of web technologies.</li>
        </ul>
    </li>
</ul>

Note:
* Have discussed video modeling, micropractice, video cases, field recordings, and video feedback
* Brief mention Video Interaction Analysis
* *Adaptive learning* - allocation human, learning, and technical resources based on unique needs of individual learner.
