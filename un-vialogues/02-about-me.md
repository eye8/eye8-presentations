## About Me

### Zhou Zhou - EdTech Architect, Developer, and Innovator
<ul>
    <li class="fragment roll-in">
        Ed.D. in Instructional Technology & Media, Teachers College Columbia University.
    </li>
    <li class="fragment roll-in">Software Engineer and educational researcher at [EdLab](https://edlab.tc.columbia.edu), Teachers College (2008-present).
        <ul>
            <li>Working cross-functionally in development, research, instructional design, and project management.</li>
            <li>Delivered 30+ projects that support teaching, learning, and research.</li>
        </ul>
    </li>
</ul>

Note:
==== 5 min ====


### Sample Projects

<ul>
    <li>
        [Vialogues <span class="fa fa-external-link"></span>](https://vialogues.com) (2009-present)<br />
        <span class="small-font">*Best Website for Teaching and Learning* 2012 by AASL.</span>
    </li><br />
    <li class="fragment roll-in">
        [New Learning Times <span class="fa fa-external-link"></span>](https://newlearningtimes.com) (2015-present)<br />
        <span class="small-font">Cited in *Ed Tech Developer's Guide* by U.S. Dept. of Education.</span>
    </li><br />
    <li class="fragment roll-in">
        [VICTOR](#/15/3) - Automated assessment of video professional discourse (2015-present)
    </li><br />
    <li class="fragment roll-in">
        [*EdLab SuperSearch*](#/15/1) - Intelligent educational search and recommendations (2016-present)
    </li>
</ul>

Note:

* Vialogues: educational video discussion tool I co-founded in 2009. Will be discussed in details.

* NLT: mobile-friendly educational publication featuring the latest news, startup companies, educational programs, and people in educational innovation.

* Automated assessment of video professional discourse: my doctoral dissertation. Used natural language processing and machine learning technologies to help evaluate professional discourses in Vialogues.

* SuperSearch: a technology solutions designed for large organizations and corporates where searching and reusing cross-departmental learning resources is difficult. It also allows machine learning components to be plugged in in order to deliver learning resources adaptively based on the context and user profiles.

* We can discuss some of these projects if there is time left after the main content of the webinar.


* Digital curriculum grant projects

  * *YoungArts Master Class* curriculum guide (2010-2014)

  * *Understanding Fiscal Responsibility* (2012-2014)

  * *Rock and Roll Forever* curriculum guide (2011-2012)

Note:

* YoungArts: HBO & National Young Arts Foundation
* UFR: Peter G. Peterson Foundation
* Rock & Roll: Rock and Roll Forever Foundation
