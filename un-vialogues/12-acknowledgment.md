## Acknowledgment

<p class="small-font text-left ninety-percent-w margin-auto">
A large part of my work in video-based learning especially the R&D of Vialogues has received the generous support from Dr. Gary Natriello, Dr. Hui Soo Chae, as well as many other current and former colleagues at the EdLab. Without their backup, there wouldn't have been Vialogues or my works on video-based learning.
</p>
