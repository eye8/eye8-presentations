## Pedagogical Models Using Video for Learning & Professional Development

1. [Video Modeling](#/6)
1. [Micropractice](#/7)
1. [Video Cases](#/8)
1. [Field Recordings / Video Feedback](#/9)
1. [Video Annotation and Video Discussion](#/10)

Note:
* Vialogues was built to solve some of the toughest challenges in video-based learning
* Over 40K users from all around the world
* Used in a wide range of disciplines of learning and PD, such as social/political studies, second-language learning, teacher education, health care education,  film studies, music studies, etc.
