## Introduction of Vialogues


<div class="streach">
    <iframe src="https://vialogues.com/vialogues/play_embedded/40355/" width="65%" height="650" frameBorder="0" scrolling="No" allowfullscreen></iframe>
</div>

Note:
==== 7 min ====
* Share recent personal experience where I find anchored video assessment and feedback is extremely helpful for learning.
* Back in 2008 I found a lot of people have similar problems in learning with videos. That was why I created Vialogues.


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2008">2008</div>
    <img class="screenshot" src="images/john-adams.jpg" style="top:175px;" />
  </div>
</div>
<div class="small-font version-container">
  <h3>Interactive Video Visualization Tool/IVVT</h3>
  <ul>
    <li>Part of the *Watch It And Learn* project with HBO/TimeWarner.</li><br />
    <li>Features
      <ul>
        <li>Create timestamped polls for the video.</li>
        <li>Define skip logic for poll responses to create dynamic video-based learning experiences.</li>
      </ul>
    </li>
  </ul>
</div>

Note:
* Vialogues has a lot of useful features for education and they don't fit in one slide
* Broke up Vialogues features based on its version timeline. And these features are progressively more advanced in more recent versions
* Vialogues originated from a grant project where EdLab was contracted to create some interactive learning tools for HBO's John Adams historical documentary, where I proposed and developed a tool for teachers to design time-based questionnaires for videos.


<div class="timeline-container small-font">
  <div class="timeline">
    <div class="timeline-year timeline-year-2009">2009</div>
    <img class="screenshot screenshot-2009" src="images/vialogues-2009.jpg" />
  </div>
</div>
<div class="small-font version-container">
  <h3>Critter</h3>
  <ul>
    <li>Timestamped asynchronous discussions and polls.</li><br />
    <li>Bring your own video or embed from YouTube/Vimeo.</li><br />
    <li>Permission control
      <ul>
        <li>Enroll participants on individual- or group-basis to private discussions.</li>
        <li>Create an intimate learning/PD community.</li>
      </ul>
    </li>
  </ul>
</div>

Note:
* Critter
  * In video-based learning people need to think critically
  * But instead of writing a full-length essay they need to speak out their reflections quickly like in Twitter
  * critical thinking + twitter
* Anchored video discussion and assessment: fix solitary and superficial learning
* Permission control:
  * privacy (children, confidential content)
  * closed community, feeling of security, encourage critical feedback


<div class="timeline-container small-font">
  <div class="timeline">
    <img class="screenshot screenshot-2012" src="images/vialogues-2012.jpg" />
    <div class="timeline-year timeline-year-2010">2010-2017</div>
    <img class="screenshot screenshot-2015" src="images/vialogues-2015.jpg" />
  </div>
</div>
<div class="small-font version-container">
  <h3>Vialogues 1.0</h3>
  <ul>
    <li>Favorite.</li><br />
    <li>Closed captioning.</li><br />
    <li>Standalone video player.</li><br />
    <li>iFrame embedding in LMS (e.g. Moodle, Wordpress, etc.) and other external websites.</li>
  </ul>
</div>

Note:
* Vialogues: video + dialogues
* Favorite: easier to come back (not just discuss once); analytics understand user better
* Closed captioning: accessibility; searchable video content
* Standalone player - easy installation in external projects; video watching event tracking
* iFrame embed
  * Contextualize video learning in authentic learning/PD contexts


<div class="timeline-container small-font">
  <div class="timeline">
    <img class="screenshot screenshot-2017" src="images/vialogues-2017.jpg" />
    <div class="timeline-year timeline-year-2017">August 2017</div>
  </div>
</div>
<div class="small-font version-container">
  <h3>Vialogues 2.0</h3>
  <ul>
    <li>Synchronous discussion and notification.</li><br />
    <li>Public groups (channels).</li><br />
    <li>*SuperSearch* integration.</li>
  </ul>
</div>

Note:
* Synchronous discussion
  * more efficient communication than asynchronous
  * possible future pedagogical models incl. video conferencing, online tutoring, webinar, etc.
* Public group
  * Content aggregation on specific topics
  * Able to use Vialogues to host MOOC courses


#### Try Out Vialogues

<div class="streach">
    <iframe src="https://vialogues.com/vialogues/play_embedded/41096/" width="60%" height="600" frameBorder="0" scrolling="No" allowfullscreen></iframe>
</div>

Note:
==== 12 min ====
