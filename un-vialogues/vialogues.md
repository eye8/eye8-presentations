## Iterative R&D of Vialogues

### Interactive Video Visualization Tool/IVVT (2008)

* Part of the *Watch It And Learn* project with HBO/TimeWarner
* Features
  * Create timestamped polls for the video
  * Create skip logic for polls to customize the video-based learning experience


### Critter (2009)

* Inherited design concepts from IVVT
* Timestamped asynchronous discussions and polls
* Bring Your Own Video
* Private discussions
  * Add user in discussion admin
  * Share secure link through email
* Private groups
  * Grant access to private discussions by group basis


### Vialogues 1.0 (2010-2017)

* Amazon Web Service full tech stack
  * Server, database, video transcoding, cloud streaming, search, emails, analytics ...
* Discuss YouTube and Vimeo videos
* Standalone video player (Flash)
* Bookmarks
* iFrame embedding on LMS, class blog, and other external websites
* Closed captioning

<aside class="notes">
  <p>AWS - cloud (smooth streaming); Elastic Transcoder (cross-platform)</p>
  <p>Standalone player - easy installation in external projects; event tracking</p>
</aside>


### Vialogues 2.0 - August 2017 Release

* JWPlayer
  * Latest HTML5 web standards
  * Support a wide range of features: interactive transcript, real-time streaming, video analytics, etc.
  * Easier integration
* Synchronous discussions, polls, and notifications
  * Higher communication efficiency
  * Support more use cases: video conferencing, online tutoring, webinar, etc.
* Public groups (channels)


### New Features Under Way

* Automatic transcript (AutoSub + Google Translation)
* Interactive, searchable transcript
* Learning Analytics
* Paid premium user (?)
