## Other Projects


### EdLab SuperSearch:
### Intelligent Educational Search and Recommendations

* Goals

    * Search messy and scattered learning resources in large organizations.

    * Serve learning resources adaptively to learners, course builders, and instructional designers.


<div class="pull-left" style="width:50%;">
    <a href="images/supersearch-concept.png" target="_blank">
        <img src="images/supersearch-concept.png" alt="SuperSearch Conceptual Design" height="350" />
    </a>
    <p class="small-font">*SuperSearch Conceptual Design*</p>
</div>
<div class="pull-right" style="width:50%;">
    <a href="images/supersearch-technical.png" target="_blank">
        <img src="images/supersearch-technical.png" alt="SuperSearch Technical Design" height="350" />
    </a>
    <p class="small-font">*SuperSearch Technical Design*</p>
</div>

Note:
* Goals
  * Adaptive support: personalized, contextualized
* Solution
  * Output can be search results or recommendations
* Ongoing project. Phase I (search engine) completed. Phase II starting soon


### Automated Assessment of Video Professional Discourse

<div class="pull-left" style="width:50%;text-align:center;">
    <ul>
        <li>Goal: automatically evaluate professional discourses in social media.</li><br />
        <li>Context: teacher professional development in Vialogues.</li>
    </ul>
</div>
<div class="pull-right" style="width:50%;text-align:center;">
    <li>Theoretical Framework</li>
    <img src="images/victor.jpg" alt="VICTOR Framework" width="350" />
    <p class="small-font">*VICTOR Model*</p>
</div>


* Solution: use natural language processing to identify the relevance between the professional discourse and learning resources (e.g. video, reading materials, assignments, etc.).

* Application

    * Assess professional discourse productivity based on its relevance to the learning resources.

    * Recommend the instructor to provide pedagogical scaffolding to unproductive professional discourses.

Note:
* Dissertation
* Context: Vialogues
* Learning materials e.g. video content, articles, textbooks, etc.
* Also applicable in other contexts without involving videos
