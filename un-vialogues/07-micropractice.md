### Micropractice

#### Purpose

* Give novice professional confidence, support, and feedback by letting them practice professional activities or problem-solving strategies with a group of real or pretended audience.

Note:
==== 8 min ====


#### Procedures

![Micropractice Procedures](images/micropractice.png)
<p class="small-font">*Video Micropractice Workflow*</p>


#### Anchored feedback for interview micropractice

<div class="streach">
    <iframe src="https://vialogues.com/vialogues/play_embedded/31622/" width="60%" height="600" frameBorder="0" scrolling="No" allowfullscreen></iframe>
</div>

Note:
* University of Colorado - Denver
* Overview of behavioral health assessment of common psychiatric disorders and medical conditions
* Students work in pairs to practice patient interview skills.
* The interviewee as well as the instructor give feedback to the interviewer on how to improve interview skills.


#### Anchored feedback for microteaching

<div class="streach">
    <iframe src="https://vialogues.com/vialogues/play_embedded/36708/" width="60%" height="600" frameBorder="0" scrolling="No" allowfullscreen></iframe>
</div>

Note:

Student teacher share coaching (microteaching) video for feedback.

"I tried my best to explain and show my colleague. Although she says she understands, I am not sure if she always does. Can you give me some feedback about how I answer her questions? Is it clear? Can it be clearer? What can I do to know for sure whether she understands the content without making her feel like she is experiencing a math quiz?"


#### Eugene piano micropractice

<div class="three-fifty-w">
    [![Eugene Piano](images/eugene.png)](#/4/1)
</div>
