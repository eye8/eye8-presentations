## Overview of Video-Based Learning

### Advantages of Video-Based Learning

<div class="small-font pull-left fragment roll-in" style="width:30%;text-align:center;">
  <img src="images/multimodality.jpg" alt="Multimodality" height="260" />
  Multimodality
</div>
<div class="small-font pull-left fragment roll-in" style="width:40%;text-align:center;">
  <img src="images/authentic-context.jpg" alt="Authentic Context" height="260" />
  Authentic Context
</div>
<div class="small-font pull-left fragment roll-in" style="width:30%;text-align:center;">
  <img src="images/thinker.jpg" alt="Critical Reflective Thinking" height="260" />
  Critical Reflective Thinking
</div>

Note:
==== 5 min ====
* Video has been introduced to professional development for several decades. Video has a lot of advantages compared to the traditional single-modal media such as text, image, and audio.
* Video is multi-modal by combining different modalities of media such as audio, animated texts, moving images, to allow information to be communicated more effectively, flexibly, and engagingly.
* Video creates the authentic contexts of learning that other types of media are not able to provide. And these contexts serve as the ground for critical reflective learning.
* In critical reflective learning, the professionals think analytically and associate the learning content to their preexisting knowledge and personal experiences.


### Critiques of Video-Based Learning

<p class="text-left">Problem 1: Video makes learning <u>passive</u>.</p>

<div class="four-twenty-five-w pull-left fragment roll-in">
  <p class="text-left">Remedies</p>
  <ul class="small-font">
    <li>Carefully designed video story-telling (e.g., <a href="https://en.wikipedia.org/wiki/Dora_the_Explorer" target="_blank">Dora The Explorer</a>)</li><br />
    <li>Pedagogical support (e.g., pre-play, repetition, learner control)</li>
  </ul>
</div>

<div class="fifty-percent-w pull-left">
  <div class="three-fifty-w">
    ![Couch Potatoes](images/couch-potato.jpg)
  </div>
</div>

Note:
* Video learning is passive because of many videos are designed with fast tempo and overwhelming multi-modal information
* Fortunately there are remedies for this problem such as better story-telling to stimulate proactive thinking
* For example -- ask questions instead of plain statement
  * Dora The Explorer (popular American educational television series by Nickelodeon)


<p class="text-left">Problem 2: Watching video face-to-face is <u>time-consuming</u> and limits <u>interpersonal interaction</u>.</p>

<div class="fifty-percent-w pull-left">
  ![Couch Potatoes](images/couch-potatoes.jpg)
</div>

<div class="four-twenty-five-w pull-right pad-top fragment roll-in">
  <p class="text-left">Remedies</p>
  <ul>
    <li>Asynchronous learning</li><br />
    <li>Flipped class</li>
  </ul>
</div>

Note:
* Asynchronous learning - students watch video at their own convenient time and conduct collaborative learning when they meet together
* This learning mode has evolved into the flipped classroom instructional model.
* Flipped class
  * Students watch lecture video before the class, and bring their questions to the class
  * During class, instructor helps students on an individual basis, and coordinate collaborative learning activities among the students.
  * Increasingly popular with the rise of MOOC (Khan Academy, Coursera, EdX, etc)


<p class="text-left">Problem 3: Video-based learning is often <u>solitary</u> and <u>superficial</u>.</p>

<div class="five-hundred-w pull-left fragment roll-in">
  <p class="text-left">Remedies</p>
  <ul>
    <li>Video annotation</li><br />
    <li>Anchored video discussion</li><br />
    <li>Anchored video assessment</li>
  </ul>
</div>

<div class="three-fifty-w pull-right">
  ![Couch Potatoes](images/video-brands.jpg)
</div>

Note:
* Nature of video technology (self-contained, difficult to combine with other learning activities such as writing, discussion, etc)
* Direct reason for creating Vialogues
