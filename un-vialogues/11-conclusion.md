## Conclusions

Note:
==== 3 min ====


<div class="five-fifty-w">
    <img src="images/video-power.png" style="border:none;background:none;" />
</div>
<div class="text-left ninety-percent-w margin-auto">
    Video is a powerful learning tool but there are limitations and pitfalls. Use effective pedagogical tools and technologies to fully release the educational power of video.
</div>


<div class="pull-left fifty-percent-w text-left">
    <br /><br />Content, pedagogy, and technology should be seamlessly integrated in order for the learning unit to be effective.
</div>

<div class="pull-left fifty-percent-w">
    <img src="images/tpack.jpg" />
    <p class="small-font">*TPACK (Koehler & Mishra, 2009)*</p>
</div>

Note:
* Don't abuse technology - use it only if it improves learning
* Make sure to assess professional learning in the intersections of T, P, and C
