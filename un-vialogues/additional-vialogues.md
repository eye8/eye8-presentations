### Future Development of Vialogues

* Alternative User Interface designs

* Automatic transcripts and translation

* Learning analytics

* Live streaming video discussion

* Business model

Note:
* Alternative UI: optimize different learning modes
  * sync vs async discussion
  * annotation vs discussion
* Automatic transcript
  * Improve searchability of video content.
  * Help international users learn.
  * AutoSub + Google Translation
* Learning analytics
  * Video playback, discussion participation, use patterns
  * Suggest instructor whether/when to intervene a discussion.
* Business model
  * Paid premium user can use advanced features.
  * Generate revenue to support continuous improvements.
