### Video Cases

#### Purpose

* Let professionals experience and reflect on typical or highly complex problem-solving cases that they often don't have access to or are unable to capture.
* Develop professional knowledge base.

Note:
==== 8 min ====


#### Film case study

<div class="streach">
    <iframe src="https://vialogues.com/vialogues/play_embedded/25213/" width="60%" height="600" frameBorder="0" scrolling="No" allowfullscreen></iframe>
</div>

Note:
* Nearly 800 Vialogues users from Syracuse since 2012. A lot from visual arts study.
* Analysis on using sound in classic movies
* 00:34: focusing on Forrest running, so running sound in foreground whereas bike sound of the bullies in background


#### Classroom management case study

<div class="streach">
    <iframe src="https://vialogues.com/vialogues/play_embedded/17680/?time=825" width="60%" height="600" frameBorder="0" scrolling="No" allowfullscreen></iframe>
</div>

Note:
* Prof. Cheryl McElvain at School of Education & Counseling Psychology, Santa Clara U
* Skip
* Tyler Hester
  * Legendary English teacher at Leadership Public School - Richmond, CA (2010-2012)
  * Teach For America > Doctoral program in Education Leadership @Harvard
  * Teaching: extremely principled (military-style), develop good learning habits for students,
  * Effective classroom management strategies
