## Outline

1. [About Me](#/2)

1. [Overview of Video-Based Learning](#/3)

1. [Introduction of Vialogues](#/4)

1. [Pedagogical Models Using Video for Learning](#/5)

1. [Conclusions](#/11)

1. [Acknowledgment](#/12)

1. [Q&A](#/13)

Note:
This is today's agenda for the webinar. I will first briefly introduce myself and my sample projects. Then I'll overview what is video-based learning, why it is important, and what the limitations are. I will introduce Vialogues, a video-based collaborative learning software I co-founded, as well as the major pedagogical models that are being used by the Vialoges users. Then after the conclusions we'll have a Q&A session.

Please feel free to post any questions you have to the Chat. I will keep an eye on them and hopefully I can answer them all throughout the presentation.


### Additional Slides

1. [List of Resources](#/14)

1. [Other Projects](#/15)

1. [Future Development of Vialogues](#/16)
