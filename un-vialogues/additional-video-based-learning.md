## Overview of Video-Based Learning

### What is *video* ?

* Video is a multi-modal medium that delivers moving images with synchronized audio and sometimes on-screen texts.

<table>
    <row>
        <td>
            <a href="images/vid-ticker.png" target="_blank">![Video Newsticker Example](images/vid-ticker.png)</a>
        </td>
        <td>
            <a href="images/vid-caption.png" target="_blank">![Video Caption Example](images/vid-caption.png)</a>
        </td>
        <td>
            <a href="images/vid-popup.png" target="_blank">![Video Pop-up Example](images/vid-popup.png)</a>
        </td>
    </row>
</table>

Note:
* Examples of on-screen texts include video captioning, scrolling news stickers in television programs, pop-up ads banner in streaming digital videos (e.g. YouTube), etc.
* Add charts to represent video definition


### Advantages of Video-Based Learning

* Video's multi-modalities allow information to be communicated more effectively, flexibly, and engagingly than single-modal media (e.g. texts, images, voice recordings, etc.).

<div class="pad-top">
    <iframe src="https://vialogues.com/videos/embedded/3394/" width="540" height="304" frameBorder="0" scrolling="No" allowfullscreen></iframe>
</div>

Note:
* Multi-modalities: audio, animated texts, moving images
* Convey info regarding the design ideas, targeted audiences, branding, company name, etc
* Strong hint for education focus by using teacher and student images
* Use of eye-catching colors, ear-catching music to make deep impression
* After watching: Ok, Vialogues created at EdLab, interesting, I want to check it out


* The scenarios represented in a video, either real or fictional, can serve as the ground for critical reflective learning.

<table>
    <row>
        <td>
            [![Classroom Video Example](images/classroom-video.png)](https://vialogues.com/vialogues/play/17680)
            <p class="small-font">EX 1. Class teaching reflection</p>
        </td>
        <td>
            [![Interview Video Example](images/interview-video.png)](https://vialogues.com/vialogues/play/33699)
            <p class="small-font">EX 2. Job interview reflection</p>
        </td>
    </row>
</table>


* Video can capture and contextualize events in their authentic contexts that may not be accessible.

<div class="pad-top">
    <iframe width="711" height="400" src="https://www.youtube.com/embed/HqmBa8FPMx8?rel=0&amp;start=55" frameborder="0" allowfullscreen></iframe>
</div>

Note:
* Advantages are affordances, not definitely better than single-modal media, and require appropriate instructional design and technical implementation


### Critiques of Video-Based Learning

* Video watching makes learning passive due to the fast-tempo and overwhelming multi-modal information.

  * Remedies
    * Better design and story-telling in video
    * Pedagogical scaffolding such as pre-play, repetition, and learner-controlled playback


* Watching videos in a face-to-face class is very time-consuming and limits teacher-student and student-student interaction time.

  * Remedies
    * Asynchronous learning (e.g. embedding video in LMS)
    * Flipped classroom


* Video-based learning is often solitary and superficial.

  * Remedies
    * Video annotation
    * Anchored video discussion
    * Anchored video assessment

Note:
* Pre-play - play a trailer or video synopsis before the video starts to let learners know what they are expecting
