### Video Annotation and Video Discussion

#### Purpose

* Contextualized, critical, and reflective learning.<br /><br />

#### How do they work?

* Provide a technology framework to:

  * Reference between the video and external resources.

  * Scaffold analytical thinking and self-observation.

  * Encourage collaborative knowledge building.

Note:
==== 8 min ====
* Does not create new pedagogical models
* Allow pedagogical models to be combined into a single learning module in order to deliver richer learning content and more effective learning experience


#### Theoretical framework

<div class="pull-left four-twenty-five-w">
    ![Video Discussion Theoretical Framework](images/video-discussion.jpg)
    <p class="small-font">*Zone of Proximal Development (ZPD) in Video-based Learning*</p>
</div>

<div class="pull-right five-hundred-w">
    <ul>
        <li>Video watching per se does not necessarily lead to cognitive/behavioral change.</li><br />
        <li class="fragment roll-in">Video discussion and annotation can help learners achieve the instructional goal by bridging the video, subject knowledge, and their preexisting knowledge.</li>
    </ul>
</div>


<div class="pull-left four-twenty-five-w">
    <ul>
        <li>A supportive learning or professional community provides feedback, guidance, and emotional support leading to cognitive and behavioral change.</li>
    </ul>
</div>

<div class="pull-right four-twenty-five-w">
    ![Professional Community](images/community.jpg)
</div>


#### Examples

* Video annotation

<div class="ninety-percent-w margin-auto">
    <p class="text-left small-font">
        <img src="images/cep.png" class="thumb" />
        <a href="#/9/2">
            <span class="thumb-name">Anchored Self-Observation On Second-Language Teaching</span>
        </a>
    </p>
    <p class="text-left small-font">
        <img src="images/forest-gump.png" class="thumb" />
        <a href="#/8/2">
            <span class="thumb-name">Film Case Study</span>
        </a>
    </p>
</div>

* Video Discussion

<div class="ninety-percent-w margin-auto">
    <p class="text-left small-font">
        <img src="images/teaching-strategies.png" class="thumb" />
        <a href="#/6/3">
            <span class="thumb-name">Multiple Perspectives Of Expert Thinking</span>
        </a>
    </p>
    <p class="text-left small-font">
        <img src="images/anchored-feedback.png" class="thumb" />
        <a href="#/7/3">
            <span class="thumb-name">Anchored Feedback For Microteaching</span>
        </a>
    </p>
</div>

Note:
* No clear boundary between video annotation and discussion
