### Video Modeling / Video Coaching

#### Purpose

* Learn expert problem-solving strategies.<br /><br />

#### Procedures

* Novice learn how experts solve problems from the video and imitate the expert problem-solving processes and strategies.

Note:
==== 8 min ====
* Monkey see monkey do.
* But there are strategies to design what to see and what to do.


#### Modeling expert *behaviors*

<div class="streach">
    <iframe src="https://vialogues.com/vialogues/play_embedded/3122/" width="60%" height="600" frameBorder="0" scrolling="No" allowfullscreen></iframe>
</div>


#### Modeling expert *behaviors and thinking*

<div class="streach">
    <iframe src="https://vialogues.com/vialogues/play_embedded/41107/" width="60%" height="600" frameBorder="0" scrolling="No" allowfullscreen></iframe>
</div>

Note:
* Sometimes video is accompanied with expert commentaries on why certain strategies are effective or ineffective.
* This helps novice to understand and learn "why" instead of simply "what" and "how"
* Dr. Herb Ginsburg at Teachers College - expert in children's developmental thinking in math
* Clinical interview skills to understand children thinking in solving math problems (counting)


#### Multiple perspectives of expert *thinking*

<div class="streach">
    <iframe src="https://vialogues.com/vialogues/play_embedded/9413/" width="60%" height="600" frameBorder="0" scrolling="No" allowfullscreen></iframe>
</div>

Note:
* The video may or may not contain expert problem-solving activities.
* Novices make productive reflection based on multiple expert perspectives and generate genuine solutions from their own angles.
