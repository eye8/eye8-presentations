## List of Resources

### Figures

<div class="ninety-percent-w margin-auto">
    <ul>

        <li>[Video Micropractice Workflow](#/7/1)</li>

        <li>[Field Recordings/Video Feedback Workflow](#/9/1)</li>

        <li>[Zone of Proximal Development (ZPD) in Video-based Learning](#/10/1)</li>

        <li>[TPACK Model](#/11/2)</li>

        <li>[SuperSearch Conceptual Design](#/15/2)

        <li>[SuperSearch Technical Design](#/15/2)

        <li>[VICTOR Model](#/15/3)</li>

    </ul>
</div>


### Vialogues

* [Eugene Sonatina in G Debug](#/4/1)

* [Get Started with Vialogues](#/4/6)

* [Red Cross Adult CPR: Check, Call & Care](#/6/1)

* [Clinical Interview with Expert Commentary](#/6/2)

* [Teaching Strategies for Effective Online Discussions](#/6/3)


* [Linka as Interviewer](#/7/2)

* [Pre Planning Coaching Session Feedback](#/7/3)

* [Forrest Gump - "Run, Forrest, Run!"](#/8/1)

* [Classroom Management - Week 1, Day 2](#/8/2)

* [CEP self-observation](#/9/2)

* [Core Practices: Teaching Video](#/9/3)

